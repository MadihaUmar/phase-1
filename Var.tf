variable "aws_access_key" {
 default = "ASIA4JFXBRZ2K3VH7IBT”
 description = "the user aws access key"
}
variable "aws_secret_key" {
 default = "SMvA4SZiBFVlXYCqIXOK60Ukzs+8YKtNRFE8uX17
 description = "the user aws secret key"
}
provider "aws" {
region     = "us-east-1"
access_key="${var.aws_access_key}"
secret_key="${var.aws_secret_key}"
}

resource "aws_instance" "web" {
  ami = "Amazon Linux 2 (64 bit x86)"
  instance_type = "t2-micro"
  vpc_security_group_ids=["${aws_security_group.allow.id}"]

  user_data = << EOF
	  	# !/bin/bash
		yum update -y
		yum install -y httpd
		systemctl start httpd
		systemctl enable httpd
		yum install -y jq
		az=$(curl --silent http://169.254.169.254/latest/dynamic/instance- identity/document| jq .availabilityZone)
		cat <<EOF > /var/www/html/index.html 
		<html><body>Hey!! This is Madiha Umar in AZ :)! </body></html>
  EOF
	
  }
  
resource "aws_vpc" "ProjVPC" {
	cidr_block = "10.200.0.0/16"
	tags = {
		Name = "ProjVPC"
	}
}

resource "aws_internet_gateway" "gw" {
	vpc_id = "${aws_vpc.ProjVPC.id}" 
	tags = {
    Name = "myIGW"
  }

}

resource "aws_subnet" "pub1" {
	vpc_id = "${aws_vpc.ProjVPC.id}"
	cidr_block = "10.200.0.0/24"
    availability_zone = "us-east-1a"
    tags = {
		Name = "public_subnet_1"
	}
}

resource "aws_subnet" "pub2" {
	vpc_id = "${aws_vpc.ProjVPC.id}"
	cidr_block = "10.200.1.0/24"
    availability_zone = "us-east-1b"
    tags = {
		Name = "public_subnet_2"
	}
}




resource "aws_subnet" "pri1" {
	vpc_id = "${aws_vpc.ProjVPC.id}"
	cidr_block = "10.200.2.0/24" 
    availability_zone = "us-east-1a"
    tags = {
		Name = "private_subnet_1"
	}
}



resource "aws_subnet" "pri2" {
	vpc_id = "${aws_vpc.ProjVPC.id}"
	cidr_block = "10.200.3.0/24" 
    availability_zone = "us-east-1b"
    tags = {
		Name = "private_subnet_2"
	}
}




resource "aws_route_table" "pubRT" {
	vpc_id = "${aws_vpc.ProjVPC.id}"
	 tags = {
		Name = "PublicRT"
	}
	route {
	cidr_block = “0.0.0.0/0”
	gateway_id = “${aws_internet_gateway.gw.id}”
}
}




resource "aws_route_table" "priRT" {
	vpc_id = "${aws_vpc.ProjVPC.id}"
	 tags = {
		Name = "PrivateRT"
	}
}

resource "aws_route_table_association" "PubSub1" {
 subnet_id = "${aws_subnet.pub1.id}"
 route_table_id = "${aws_route_table.pubRT.id}"
}

resource "aws_route_table_association" "PubSub2" {
   subnet_id = "${aws_subnet.pub2.id}"
  route_table_id = "${aws_route_table.pubRT.id}"
}



resource "aws_route_table_association" "PriSub1" {
 subnet_id = "${aws_subnet.pri1.id}"
 route_table_id = "${aws_route_table.priRT.id}"
}

resource "aws_route_table_association" "PriSub2" {
   subnet_id = "${aws_subnet.pri2.id}"
  route_table_id = "${aws_route_table.priRT.id}"
}


resource "aws_security_group" "allow" {
	name = "project-sg-1"
	vpc_id =  "${aws_vpc.ProjVPC.id}"

  ingress {
    port   = ["80", "22"]
    protocol    = ["http", "ssh"]
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "project-sg-1"
  }
}

resource "aws_key_pair" "keyPair" {
   key_name   = "project_key_pair"
}


resource "aws_nat_gateway" "NATgw" {
  allocation_id = "${aws_eip.nat.id}"
  subnet_id     = ["${aws_subnet.pub1.id}","${aws_subnet.pub2.id}"]

  tags = {
    Name = "NAT gw"
  }
}



resource "aws_autoscaling_group" "auto" {
  availability_zones = ["us-east-1"]
  desired_capacity   = 1
  max_size           = 1
  min_size           = 1

  launch_template {
    id      = "${aws_launch_template.Customized.id}"
    version = "$Latest"
  }
}


resource "aws_lb_target_group" "target_grp" {
  name     = "ProjTarGroup"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.ProjVPC.id}"
}


resource "aws_lb" "ProjLoadBaancer" {
  name               = "ProjLB"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.allow.id}"]
  subnets            = ["${aws_subnet.pub1.id}","${aws_subnet.pub2.id}"]

}

output "webapp_elb_name" {
value = "${aws_elb.webapp_elb.name}"
}

data "aws_launch_template" "Customized" {

  name = "ProjTemplate"
  ami  = "Amazon Linux 2 (64 bit x86)"
  instance_type = "t2-micro"
  key_name = "aws_key_pair.KeyPair.id"
  subnet_id = ["${aws_subnet.pub2.id}","${aws_subnet.pub2.id}"]
  vpc_security_group_ids = "${aws_security_group.allow.id}"

}

